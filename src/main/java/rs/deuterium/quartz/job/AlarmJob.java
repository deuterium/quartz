package rs.deuterium.quartz.job;

import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * Created by MilanNuke 08-Sep-19
 */

@Component
public class AlarmJob extends QuartzJobBean {

    private static final Logger logger = LoggerFactory.getLogger(AlarmJob.class);

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        logger.info("Executing Job with key {}", jobExecutionContext.getJobDetail().getKey());

        JobDataMap jobDataMap = jobExecutionContext.getMergedJobDataMap();
        String subject = jobDataMap.getString("subject");
        String body = jobDataMap.getString("body");
        String execTime = LocalDateTime.now().toString();

        logger.info("Alarm: {} - {} executed at: {}", subject, body, execTime);
    }
}
