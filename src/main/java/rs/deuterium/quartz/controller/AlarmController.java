package rs.deuterium.quartz.controller;

import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.deuterium.quartz.job.AlarmJob;
import rs.deuterium.quartz.model.AlarmRequest;
import rs.deuterium.quartz.model.AlarmResponse;

import javax.validation.Valid;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.UUID;

/**
 * Created by MilanNuke 08-Sep-19
 */

@RestController
@RequestMapping("/alarms")
public class AlarmController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private Scheduler scheduler;

    @PostMapping
    public ResponseEntity<?> setNewAlarm(@RequestBody @Valid AlarmRequest request){
        logger.info("In the TEST");

        try {
            ZonedDateTime dateTime = ZonedDateTime.of(request.getDateTime(), request.getTimeZone());
            if(dateTime.isBefore(ZonedDateTime.now())) {
                AlarmResponse response = new AlarmResponse(false, "dateTime must be after current time");
                return ResponseEntity.badRequest().body(response);
            }

            JobDetail jobDetail = buildJobDetail(request);
            Trigger trigger = buildJobTrigger(jobDetail, dateTime);
            scheduler.scheduleJob(jobDetail, trigger);

            AlarmResponse response = new AlarmResponse(
                    true,
                    jobDetail.getKey().getName(),
                    jobDetail.getKey().getGroup(),
                    "Alarm Scheduled Successfully!");

            return ResponseEntity.ok(response);
        } catch (SchedulerException ex) {
            logger.error("Error scheduling email", ex);

            AlarmResponse response = new AlarmResponse(false,"Error scheduling alarm. Please try later!");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }

    }

    private JobDetail buildJobDetail(AlarmRequest request) {
        JobDataMap jobDataMap = new JobDataMap();

        jobDataMap.put("subject", request.getSubject());
        jobDataMap.put("body", request.getBody());

        return JobBuilder.newJob(AlarmJob.class)
                .withIdentity(UUID.randomUUID().toString(), "alarm-jobs")
                .withDescription("Alarm Job")
                .usingJobData(jobDataMap)
                .storeDurably()
                .build();
    }

    private Trigger buildJobTrigger(JobDetail jobDetail, ZonedDateTime startAt) {
        return TriggerBuilder.newTrigger()
                .forJob(jobDetail)
                .withIdentity(jobDetail.getKey().getName(), "alarm-triggers")
                .withDescription("Alarm Trigger")
                .startAt(Date.from(startAt.toInstant()))
                .withSchedule(SimpleScheduleBuilder.simpleSchedule().withMisfireHandlingInstructionFireNow())
                .build();
    }

}
