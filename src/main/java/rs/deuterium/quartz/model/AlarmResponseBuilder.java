package rs.deuterium.quartz.model;

/**
 * Created by MilanNuke 08-Sep-19
 */
public final class AlarmResponseBuilder {
    private boolean success;
    private String jobId;
    private String jobGroup;
    private String message;

    private AlarmResponseBuilder() {
    }

    public static AlarmResponseBuilder anAlarmResponse() {
        return new AlarmResponseBuilder();
    }

    public AlarmResponseBuilder withSuccess(boolean success) {
        this.success = success;
        return this;
    }

    public AlarmResponseBuilder withJobId(String jobId) {
        this.jobId = jobId;
        return this;
    }

    public AlarmResponseBuilder withJobGroup(String jobGroup) {
        this.jobGroup = jobGroup;
        return this;
    }

    public AlarmResponseBuilder withMessage(String message) {
        this.message = message;
        return this;
    }

    public AlarmResponse build() {
        return new AlarmResponse(success, jobId, jobGroup, message);
    }
}
